import { ready } from "./subscripts/utils";
import MatchingGame from "./subscripts/matchingGame";
import polyfill from "template-polyfill";
polyfill();

ready(() => {
  window.game = new MatchingGame(18, 180, false);
});
