# ReSoMaG

**R**e**S**o**M**a**G**: yet another **R**eally **S**imple **M**atching **G**ame

The scores are saved in a database, using the [ReSoMaG-Server API](https://github.com/borisschapira/ReSoMaG-Server).

Sound effects are a courtesy from https://www.zapsplat.com. The restart SVG comes from NounProject (Boris' private licence).

## Development

## Prerequisites

- nodejs[^nvm] (tested with v12.x and v13.x)  
  (_I strongly recommend the use of [nvm](https://github.com/nvm-sh/nvm)_)

## Installation

Clean install the project using `npm ci`.

The project is using:

- `webpack`: to handle the JS script build (supports ES6 arrow functions, classes, and compile them with `babel` for browsers that don't support ES6 – see [.browserlistrc](./.browserlistrc) for supported browsers).
- `node-sass`, `postcss` and `autoprefixer` to build the CSS and prefix the properties that need it (like `backface-visibility` for Safari).

## Live Coding

- `npm run start`: uses `webpack-dev-server` to watch the JS in `src`, build it and expose a web server exposing the `dist` folder
- `npm run watchcss`: watches the CSS file in `src` and building it inside `dist`

You can use both commands simultaneously, or modify the project to use WebPack to do both (I didn't for performance reasons, as WebPack is slower when tracking SASS files).

To activate the `devMode` in the browser, open the Dev Tools and execute in the console: `game.devMode = true;`.

## API Structure

The `MatchingGame` is the entry point of the project. The constructor takes 3 parameters: the number of pairs to match (up to 18), the allocated time in secondes (default: 180) and a third parameter to active the Developer Mode (that displays card numbers on the back, to cheat, and offers more logs in the console).

![](devmode.png)

## Deployment

### Prerequisites

- rsync

### via the CLI

**From your computer**: execute `npm run deploy` to copy the files to the server via `rsync`.
